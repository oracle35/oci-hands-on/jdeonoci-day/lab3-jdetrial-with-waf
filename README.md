# lab3-jdetrial-with-waf

Secure the load-balancer with Web Application Firewall

## Getting started

The objective of this lab is to secure the load-balancer configured in Lab 2 by adding Web Application rules and filter incoming requests.

## target architecture

  ![JDE Trial Architecture with waf](./images/jdetrial-waf-architecture.png)

## lab steps

### Task 1. create a WAF Policy

In this step, we will provision a Web Application Policy

1. Sign in to OCI tenancy; from the Oracle CLoud Console home page, click the **Navigation** Menu Button in the upper-left corner and hover over **Identity & Security** and select **Web Application Firewall->Policies**.

    ![waf-menu](./images/waf-menu.png)

2. Click **Create WAF Policy**

    ![waf-create](./images/waf-create.png)

3. Enter the Basic Information:

    * Name: ***waf_jde***
    * WAF Policy Compartment: ***existing compartment***

    ![waf-basic](./images/waf-basic.png)

4. Define the protection rules available for the WAF according to your security requirements:

    * Access Control (Request & Response): define access rules based on URL path, headers, cookies, country, source IP, host, method ...
    * Rate Limiting: limite the number of requests per seconds per URL path
    * Protections: choose in the list of 350+ predefined protection capabilities to secure the incoming requests:

    ![waf-protections](./images/waf-protections.png)

    ![waf-protections](./images/waf-protections2.png)

5. Select the enforcement point:

    * Create the Firewall attached to the load-balancer created in Lab 2

    ![waf-elb](./images/waf-elb.png)

6. Review the configuration and validate with the **Create WAF Policy button**

    ![waf-create-end](./images/waf-create-end.png)

When the WAF Policy is active, your load-balancer is secured with the protection rules you attached to it.

  ![waf-elb-attached](./images/waf-elb-attached.png)

